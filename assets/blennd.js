jQuery(document).ready(function($) {

  function get_next_posts() {
    url = "/wp-json/NathanDJohnson/Blennd/get_next_posts/[" + posts + "]";
    jQuery.ajax({
      url: url,
    })
    .done(function(data){
      if( data.length === 0) {
        $("#load-more").remove();
        return;
      }
      $("#blennd-recent-posts").data("nextposts",data);
    });
  }

  $("#blennd-recent-posts .transparent").removeClass("transparent");

  posts = $("#blennd-recent-posts").data("posts");
  get_next_posts();

  $( "#load-more" ).click(function(){
    $("#blennd-recent-posts").data("nextposts").forEach(function(id){
      url = "/wp-json/NathanDJohnson/Blennd/get_post/" + id;
      $.ajax({
        url: url,
      })
      .done(function(data){
        posts.push(data.ID);
        $("#blennd-recent-posts").data("posts",posts);
        $("#blennd-recent-posts").append(data.blennd_rendered);
        setTimeout(function(){
          $("#blennd-recent-posts .transparent").removeClass("transparent");
        },50);

        next_posts = get_next_posts();
      });
    });
  });
});
