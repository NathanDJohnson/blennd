<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for displaying a WordPress shortcode.
 *
 * @since 1.0.0
 */
class shortcode {

  /**
   * Return the output of the [blennd-posts] shortcode.
   *
   * @access public
   * @since  1.0.0
   */
  public function display( $atts, $content = '' ) : string {

    require_once __DIR__ . '/render.php';
    require_once __DIR__ . '/query.php';

    $query = new query( [
      'post_type' => 'post',
      'posts_per_page' => 2,
    ]);

    $render = new render( $query );
    return $render->output();
  }
}
