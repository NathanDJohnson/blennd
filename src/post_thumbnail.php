<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for extending the WordPress post thumbnail.
 *
 * @since 1.0.0
 */
class post_thumbnail {

  /**
   * Return a default image to display in the Blennd Recent Posts shortcode if
   * one doesn't exist.
   *
   * @access public
   * @since  1.0.0
   */
  public function html( string $html ) : string {

    if( '' !== $html ) {
      return $html;
    }

    return sprintf( '<img width="400" height="300" src="%1$s" class="blank-featured-image" alt="Blank featured image">',
      \plugins_url( '/assets/default.png', __DIR__ )
    );
  }
}
