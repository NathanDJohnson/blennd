<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for interacting with stylesheets the WordPress way.
 *
 * @since 1.0.0
 */
class style {

  /**
   * Enqueue the Blennd CSS file.
   * Should be run on the `wp_enqueue_scripts` hook.
   *
   * @access public
   * @since  1.0.0
   */
  public function enqueue() {
    \wp_enqueue_style( 'blennd', \plugins_url( '/assets/blennd.css', __DIR__ ) );
  }
}
