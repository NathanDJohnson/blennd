<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for extending the WordPress REST API.
 *
 * @since 1.0.0
 */
class rest_api {

  /**
   * Register two additional REST routes.
   * Should be run on the `rest_api_init` hook.
   *
   * @access public
   * @since  1.0.0
   */
  public function init() {
    \register_rest_route( 'NathanDJohnson/Blennd', '/get_next_posts/(?P<ids>[\[\],0-9]+)', [
      'methods' => 'GET',
      'callback' => [ $this, 'get_next_posts' ],
    ] );

    \register_rest_route( 'NathanDJohnson/Blennd', '/get_post/(?P<id>\d+)', [
      'methods' => 'GET',
      'callback' => [ $this, 'get_post' ],
    ] );
  }

  /**
   * Send a JSON response containing the post object given the post ID.
   *
   * @access public
   * @since  1.0.0
   */
  public function get_post( \WP_REST_Request $request ) {
    require_once __DIR__ . '/post.php';

    $post = \get_post( $request[ 'id' ] );
    $post->blennd_rendered = ( new post( intval( $request[ 'id' ] ) ) )->output();
    \wp_send_json( $post );
    \wp_die();
  }

  /**
   * Send a JSON response containing the next two posts to display given an
   * array of posts already displayed.
   *
   * @access public
   * @since  1.0.0
   */
  public function get_next_posts( \WP_REST_Request $request ) {
    $ids = \json_decode( $request[ 'ids' ] );

    require_once __DIR__ . '/query.php';

    $query = new query( [
      'post_type' => 'post',
      'posts_per_page' => 2,
      'post__not_in' => $ids,
    ] );

    \wp_send_json( $query->array_ids() );
    \wp_die();
  }
}
