<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for formatting the posts.
 *
 * @since 1.0.0
 */
class post {

  protected $id;

  /**
   * Constructor.
   *
   * @access public
   * @since  1.0.0
   */
  public function __construct( int $id ) {
    $this->id = $id;
  }

  /**
   * Return the formatted post categories.
   *
   * @access protected
   * @since  1.0.0
   */
  protected function get_the_categories() : string {
    $categories = \get_the_category( $this->id );
    $names = [];
    foreach( $categories as $category ) {
      $names[] = $category->name;
    }
    return implode( ', ', $names );
  }

  /**
   * Return the formatted post.
   *
   * @access public
   * @since  1.0.0
   */
  public function output() : string {

    return \sprintf( '
      <article class="article transparent">
        <div class="post-thumbnail">%1$s</div>
        <div class="post-content">
          <div class="post-category">
            <p>%2$s</p>
          </div>
          <div class="post-title">
            <h4>%3$s</h4>
          </div>
          <div class="post-excerpt">
            <p>%4$s</p>
          </div>
          <div class="read-more-link">
            <a href="%5$s">Read more</a>
          </div>
        </div>
      </article>',
      \get_the_post_thumbnail( $this->id, 'blennd_featured' ),
      $this->get_the_categories(),
      \get_the_title( $this->id ),
      \get_the_excerpt( $this->id ),
      \get_the_permalink( $this->id )
    ) ;
  }
}
