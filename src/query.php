<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for setting a custom WordPress query.
 *
 * @since 1.0.0
 */
class query {

  /**
   * @var array Array of post IDs returned from the WP_Query.
   *
   * @access public
   * @since  1.0.0
   */
  public $post_ids;

  /**
   * @var array The actual WP_Query object.
   *
   * @access public
   * @since  1.0.0
   */
  public $wp_query;

  /**
   * Constructor.
   *
   * @access public
   * @since  1.0.0
   */
  public function __construct( array $atts = [] ) {
    $this->wp_query = new \WP_Query( $atts );
    $this->post_ids = $this->post_ids( $this->wp_query );
  }

  /**
   * Reset the WordPress postdata.
   *
   * @access public
   * @since  1.0.0
   */
  public function reset_postdata() {
    \wp_reset_postdata();
  }

  /**
   * Return an array of post ID from post_ids property.
   *
   * @access public
   * @since  1.0.0
   */
  public function array_ids() : array {
    if( '' === $this->post_ids ) {
      return [];
    }
    $ids = explode( ',', $this->post_ids );
    foreach( $ids as $key => $id ) {
      $ids[ $key ] = intval( $id );
    }
    return $ids;
  }

  /**
   * Return a string of comma delimited post IDs from the \WP_Query object.
   *
   * @access protected
   * @since  1.0.0
   */
  protected function post_ids() : string {
    $return = '';
    foreach( $this->wp_query->posts as $post ) {
      $return = $return . $post->ID . ',';
    }
    return trim( $return, ',' );
  }
}
