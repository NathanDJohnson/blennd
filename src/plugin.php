<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for bootstrapping the plugin.
 *
 * @since 1.0.0
 */
class plugin {

  /**
   * Require the necessary files to bootstrap the plugin and add necessary callbacks.
   * This method should be called on the `init` hook.
   *
   * @access public
   * @since  1.0.0
   */
  public function init() {

    //* Adds the [blennd-posts] shortcode.
    require_once __DIR__ . '/shortcode.php';
    \add_shortcode( 'blennd-posts', [ new shortcode(), 'display' ] );

    //* Adds a custom image size for featured images.
    require_once __DIR__ . '/media.php';
    \add_action( 'after_setup_theme', [ new media(), 'add_image_size' ] );

    //* Enqueues Blennd JavaScript.
    require_once __DIR__ . '/style.php';
    \add_action( 'wp_enqueue_scripts', [ new style(), 'enqueue' ] );

    //* Enqueues Blennd CSS.
    require_once __DIR__ . '/script.php';
    \add_action( 'wp_enqueue_scripts', [ new script(), 'enqueue' ] );

    //* Adds a default post thumbnail image.
    require_once __DIR__ . '/post_thumbnail.php';
    \add_filter( 'post_thumbnail_html', [ new post_thumbnail(), 'html' ] );

    //* Adds REST API endpoints.
    require_once __DIR__ . '/rest_api.php';
    \add_filter( 'rest_api_init', [ new rest_api(), 'init' ] );

  }
}
