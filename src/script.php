<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for interacting with JavaScript the WordPress way.
 *
 * @since 1.0.0
 */
class script {

  /**
   * Enqueue the blennd.js JavaScript file.
   * Should be run on the `wp_enqueue_scripts` hook.
   *
   * @access public
   * @since  1.0.0
   */
  public function enqueue() {
    \wp_enqueue_script( 'blennd', \plugins_url( '/assets/blennd.js', __DIR__ ), [ 'jquery' ] );
  }
}
