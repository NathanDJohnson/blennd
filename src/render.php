<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for rendering the output.
 *
 * @since 1.0.0
 */
class render {

  protected $query;

  /**
   * Constructor.
   *
   * @access public
   * @since  1.0.0
   */
  public function __construct( \NathanDJohnson\Blennd\query $query ) {
    $this->query = $query;
  }

  /**
   * Return the formatted [blennd-posts] shortcode output.
   *
   * @access public
   * @since  1.0.0
   */
  public function output() : string {
    require_once __DIR__ . '/post.php';

    $output = sprintf( '
        <section id="blennd-recent-posts" class="blennd-recent-posts" data-nextposts=[] data-posts=[%1$s]>
          <h3>Blennd Recent Posts</h3>
          <div class="load-more-link"><a id="load-more">Load more</a></div>',
          $this->query->post_ids
        );

    if( $this->query->wp_query->have_posts() ) {
      while( $this->query->wp_query->have_posts() ) {
        $this->query->wp_query->the_post();

        $post = new post( \get_the_ID() );
        $output .= $post->output();
      }
      $output .= '
        </section>';
    }
    else {
      $output .= '<p>There are no recent posts. Check back soon!</p>';
    }

    $this->query->reset_postdata();
    return $output;
  }
}
