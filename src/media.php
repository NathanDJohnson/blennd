<?php

/**
 * @package NathanDJohnson\Blennd
 */
declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

/**
 * Class for interacting with the WordPress media library.
 *
 * @since 1.0.0
 */
class media {

  /**
   * Add images sizes to the WordPress media library.
   *
   * @access public
   * @since  1.0.0
   */
  public function add_image_size() {
    \add_image_size( 'blennd_featured', 300, 200, true );
  }
}
