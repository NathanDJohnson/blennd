# Blennd Code Challenge

## Description

A solution to the Blennd Code Challenge.

## Requirements

- WordPress 5.2 or greater
- PHP 7.0 or greater

## Installation

1. [Download](https://gitlab.com/NathanDJohnson/blennd/repository/archive.zip) the project ZIP file
2. Extract the ZIP file in your wp-content/plugins/ directory
3. Rename the extracted directory to 'blennd'
4. Navigate in your browser to your wp-admin/plugins.php page and activate the plugin

## Documentation
Add the [blennd-posts] shortcode to a page or post. That's it.
