<?php

/**
 * Plugin Name:       Blennd Code Challenge
 *
 * @package           NathanDJohnson\Blennd
 * @author            Nathan Johnson
 * @copyright         2019 Nathan Johnson
 * @license           GPL-2.0-or-later
 *
 * Plugin URI:        https://gitlab.com/NathanDJohnson/blennd/
 * Description:       A solution to the Blennd Code Challenge.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.0
 * Author:            Nathan Johnson
 * Author URI:        https://gitlab.com/NathanDJohnson/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 */

declare( strict_types = 1 );
namespace NathanDJohnson\Blennd;

require_once __DIR__ . '/src/plugin.php';
\add_action( 'plugins_loaded', [ new plugin(), 'init' ] );
